import numpy as np

'''
A collection of functions for transforming the data.
'''


def expand(labels):
    # Takes a vector of labels and transforms it into a matrix of one-hot encodings
    label_matrix = np.zeros((labels.size, max(labels)+1))
    for line in range(labels.shape[0]):
        label_matrix[line][labels[line]] = 1
    return label_matrix