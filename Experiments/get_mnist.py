import gzip
import pickle
import DataAccess.data_transform

def load_data(dataset):
    ''' Loads the dataset

    :type dataset: string
    :param dataset: the path to the dataset (here MNIST)
    '''
    #############
    # LOAD DATA #
    #############

    # Load the dataset
    f = gzip.open(dataset, 'rb')
    u = pickle._Unpickler(f)
    u.encoding = 'latin1'
    train_set, valid_set, test_set = u.load()
    f.close()
    train_set_x = train_set[0]
    train_set_y = train_set[1]
    valid_set_x = valid_set[0]
    valid_set_y = valid_set[1]
    test_set_x = test_set[0]
    test_set_y = test_set[1]

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),(test_set_x, test_set_y)]
    return rval

rval = load_data('../../../datasets/mnist.pkl.gz')
train_set_x, train_set_y = rval[0]
valid_set_x, valid_set_y = rval[1]
test_set_x, test_set_y = rval[2]
train_set_y = DataAccess.data_transform.expand(train_set_y)
valid_set_y = DataAccess.data_transform.expand(valid_set_y)
test_set_y = DataAccess.data_transform.expand(test_set_y)
used_set_x = train_set_x # The set actually used for training
used_set_y = train_set_y
np.savez("train_set", X=train_set_x)