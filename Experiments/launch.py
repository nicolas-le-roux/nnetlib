# Base script to launch an experiment.

# Load the dataset.
import sys
from Core.losses import *
from Models.Nnet import Nnet
from Optimizer.Optimizer import Optimizer
from Optimizer.Solver import Solver
import numpy as np

tempdata = np.load("../../../datasets/train_set.npz")
used_set_x = tempdata['X'][:, :-1]
used_set_y = np.log(tempdata['X'][:, -1])


tempdata = np.load("../../../datasets/validation_set.npz")
valid_set_x = tempdata['X'][:, :-1]
valid_set_y = np.log(tempdata['X'][:, -1])

model = Nnet(input_size=used_set_x.shape[1], output_size=1, activation=['pos','none'], hidden_sizes=[1000, 1000])
solver = Solver("l2_loss", model, l1_regularizer=0, l2_regularizer=0.0001)
optimizer = Optimizer(solver, method='sag', read_type="random", stepsize=-1.0, max_updates=50000, minibatch=50, display=500, max_data=sys.maxsize)
optimizer = Optimizer(solver, method='layer_newton', read_type="random", stepsize=-1.0, max_updates=50000, minibatch=50000,display=1, max_data=sys.maxsize)
optimizer.train(used_set_x, used_set_y)

# Predict on the train set.
model.predict_batch(used_set_x)
training_nll = np.mean(log_loss(model.output, used_set_y))
training_classif_error = np.mean(classif_loss(model.output, used_set_y))
print("Single train NLL={}, Single train classif error={}".format(training_nll, training_classif_error))

model.predict_batch(valid_set_x)
valid_nll = np.mean(log_loss(model.output, valid_set_y))
valid_classif_error = np.mean(classif_loss(model.output, valid_set_y))
print("Single valid NLL={}, Single valid classif error={}".format(valid_nll, valid_classif_error))
